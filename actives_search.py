#Данный модуль предназначен для поиска и фильтрации необходимых активов в базе данных

import neuthink.graph.basics as bs
from typing import Union, Dict, List, Optional, Any
from neuthink import metatext as txt




def get_num(percent: float, num: int) -> int:
    '''
    Возвращает число от процента
    
    Вернуть процент(percent) от числа(num). Округлить в большую сторону, если число после точки >0.5

    >>> get_num(10.0, 100)
    10

    >>> get_num(30.0, 12)
    4
    '''
    result = (num / 100 * percent)
    delta = result % 1
    end_result = (result - delta) if delta < 0.5 else (result - delta + 1)
    return int(end_result)


def country_checker(active_type: str, country: str, num: int) -> bool:
    '''
    Проверяет наличие нужного количества активов в списке узлов actives.

    Вернуть True если узлов с полем country равным country больше или равно num, иначе False
    '''
    result = len(gr.Match({'type': active_type, 'country': country})) >= num
    return result

#2 часть


def search_by_type(active_type: str, num: int) -> bs.NodeList:
    '''
    Находит необходимое количество узлов с самым низким ivar

    Вернуть num узлов с самым низким значением поля ivar и type равным переменнной type.
    '''
    node_list = gr.Match({'type': active_type})
    result = bs.NodeList(gr, sorted(node_list, key=for_sort))[0: num]
    return result


def search_by_country(active_type: str, country: str, num: int) -> bs.NodeList:
    '''
    Возвращает необходимое количество узлов среди узлов activelist

    Возвращает num узлов с самым низким значением поля ivar и полем country равным country среди узлов activelist.
    '''
    node_list = gr.Match({'type': active_type, 'country': country})
    result = bs.NodeList(gr, sorted(node_list, key=for_sort))[0: num]
    return result

#3 часть


def get_country(countryDict: Dict[str, float], num: int) -> Dict[str, int]:
    '''
    Проверяет введённые страны. Возвращает словарь с новыми значениями
    
    С помощью функции get_num сформировать новый словарь.
    
    a = {'France':50.0, 'China':50.0}
    b = {'Russia':100.0}

    >>> get_country(a, 30)
    {'France':15, 'China':15}

    >>> get_country(b, 5)
    {'Russia':5}
    '''
    new_dict = {country: get_num(countryDict[country], num) for country in countryDict}
    return new_dict


def types_counter(types_list: List[str], actives: Dict[str, Dict[str, Union[float, Dict[str, float]]]]) -> int:
    '''
    Функция расчитывает сколько элементов из списка type_list содержится в словаре actives

    Перебирает элементы списка type_list и увеличивает счетчик counter на один, если элемент найден в словаре
    '''
    counter = 0
    
    for active_type in types_list:
        counter = (counter + 1) if active_type in actives else counter
    
    return counter


def check_percentage(actives: Dict[str, Dict[str, Union[float, Dict[str, float]]]], num: int) \
        -> Union[bool, Dict[str, Dict[str, Union[int, Dict[str, int]]]]]:
    '''
    Проверяет правильность введённых активов.

    Если в словаре содержаться forex, crypto и company - проверить, что сумма их значений по ключу 'percent' равна 100. Иначе ошибка
    Если в словаре два актива - Проверить, что их сумма не превышает 100. Иначе ошибка
    Если в словаре одно из значений - проверить, что оно не превышает 100.
    Если соблюдены условия верны для каждого из случаев - вернуть True, иначе False

    a = {'forex':{'percent':20.0}, 'company':{'percent':60.0}, 'crypto':{'percent':20.0}}
    b = {'forex':{'percent':30.0}, 'company':{'percent':310.0}}

    >>> check_percentage(a)
    True

    >>> check_percentage(b)
    False
    '''
    types_list = ['forex', 'company', 'crypto']
    result_dict: Dict[str, Dict[str, Union[int, Dict[str, int]]]] = {}
    amount = types_counter(types_list, actives)
    percents = sum([active['percent'] for active in actives.values() if isinstance(active['percent'], float)])

    if (amount == 3 and float(percents) == 100) or (1 <= amount <= 2 and float(percents) <= 100):
        
        for active in types_list:
            percent = actives[active]['percent'] if active in actives else (100 - percents)/(3 - amount)
            if isinstance(percent, float):
                result_dict[active]= {'count': get_num(percent, num)}
        return result_dict
    return False

def count_actives(actives: Dict[str, Dict[str, Union[float, Dict[str, float]]]], num: int) -> \
        Union[bool, Dict[str, Dict[str, Union[int, Dict[str, int]]]]]:
    '''
    Возвращает словарь с числом активов

    Проверить условия с помощью check_percentage, вернуть ошибку если False.
    Для всех percent получить новое значение get_num, занести в словарь.
    Если в словаре есть ключ 'country', с помощью get_country так же получить значения и занести в словарь.

    a = {'forex':{'percent':20.0}, 'company':{'percent':60.0}, 'crypto':{'percent':20.0}}
    b = {'forex':{'percent':30.0, 'country':{'France':50.0, 'China':50.0}}}
    c = {'forex':{'percent':30.0}, 'company':{'percent':30.0}}
    >>> count_actives(a, 30)
    {'forex':{'count':6}, 'company':{'count':18}, 'crypto':{'count':6}}

    >>> count_actives(b, 51)
    {'forex': {'count': 15, 'country': {'France': 8, 'China': 8}}, 'company': {'count': 18}, 'crypto': {'count': 18}}'

    >>> count_actives(c, 100)
    {'forex':{'count':30}, 'company':{'count':30}, 'crypto':{'count':40}}
    '''
    indicator = check_percentage(actives, num)
    if isinstance(indicator, bool):
        return False
    else:
        result_dict = indicator
        for active in actives:
            if 'country' in actives[active]:
                if isinstance(result_dict, dict):
                    first = actives[active]['country']
                    sec = result_dict[active]['count']
                    if isinstance(first, dict) and isinstance(sec, int):
                        result_dict[active]['country'] = get_country(first, sec)
        return result_dict


def add_country_nodes(active: str, active_value: Dict[str, Union[int, Dict[str, int]]], result_dict: Dict[str, bs.NodeList]) -> None:
    '''
    Добавляет узлы по странам в итоговый список

    Получает количество необходимых узлов country_count, которые нужно получить для каждой страны указанной в поле 'country'.
    Проверяет с помощью country_checker достаточно ли необходимых узлов: если нет - выводит соответствующее сообщение.
    Получает узлы с помощью search_by_country и добавляет их в итоговый список
    '''
    if isinstance(active_value['country'], dict):
        for country in active_value['country']:
            country_count = active_value['country'][country]
            if country_checker(active, country, country_count) is False:
                print(f'Недостаточно узлов типа {active} по стране {country}')
            result_dict[active] += search_by_country(active, country, country_count)
    pass


def get_actives(ActivesDict: Dict[str, Dict[str, Union[int, Dict[str, int]]]]) -> Dict[str, bs.NodeList]:
    '''
    Получить необходимое количество узлов

    Названия активов - ключи словаря ActivesDict. Количество - значение 'count' внутри значения ключа.
    Вернуть словарь, где ключом является название актива, а значением список узлов, полученный из функции search_by_type.
    Если в словаре есть ключ country, дополнительно отсортировать значения с помощью search_by_country, если country_checker == True, иначе ошибка.
    Значения не суммируются, а вычитаются (count - country), если значений country меньше, чем count - дополнить до нужного количества узлами из search_by_type.
    '''
    result_dict = {'forex': bs.NodeList(gr, []), 'company': bs.NodeList(gr, []), 'crypto': bs.NodeList(gr, [])}

    for active in ActivesDict:
        count = ActivesDict[active]['count']

        if 'country' in ActivesDict[active]:
            add_country_nodes(active, ActivesDict[active], result_dict)
        node_numbers = len(result_dict[active])

        if isinstance(count, int):
            if node_numbers < count:
                result_dict[active] += search_by_type(active, (count - node_numbers))

    return result_dict


def search_actives(activesDict: Dict[str, Dict[str, Union[float, Dict[str, float]]]], num: int, what_ivar:str) -> Optional[Dict[str, Any]]:
    '''
    Получает исходный словарь с активами, распределяет по количеству num, возвращает отфильтрованный результат.

    Получает количество каждого актива от count_actives, фильтрует в get_actives и возвращает результат.
    '''
    if what_ivar == 'full_ivar':
        csv_name = 'ivar_3.csv'
    else:
        csv_name = 'ivar_15.csv'
    gr = txt.LoadCSV(csv_name).parent_graph
    for_sort = lambda node: node['iVaR']
    newdict = {}
    actives = count_actives(activesDict, num)
    if not isinstance(actives, bool):
        result = get_actives(actives)
        for x in result:
            newdict[x] = [dict(y) for y in result[x]]
        return newdict
    
    else:
        raise ValueError('Суммарное значение всех полей "percent" не соответствует 100%')
        return None
        

if __name__ == '__main__':
    a: Dict[str, Dict[str, Union[float, Dict[str, float]]]] = {'company':{'percent':75.0, 'country':{'BM':30.0, 'US':30.0, 'CN':30.0}}}
    b = {'forex':{'percent':100.0}}
    print(search_actives(a, 5, 'half_ivar'))