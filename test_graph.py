from neuthink.graph.basics import Graph, Node

gr = Graph()

Node(gr, {'type':'forex', 'country':'Russia', 'name':"Ruble", 'iVaR':0.1})
Node(gr, {'type':'forex', 'country':'Russia', 'name':"Ruble", 'iVaR':1.0})
Node(gr, {'type':'forex', 'country':'China', 'name':"Yuan", 'iVaR':1.92})
Node(gr, {'type':'forex', 'country':'China', 'name':"Dollar", 'iVaR':1.02})
Node(gr, {'type':'forex', 'country':'Germany', 'name':"Euro", 'iVaR':1.900001})

Node(gr, {'type':'crypto', 'country':'Russia', 'name':"Ethereum", 'iVaR':0.0})
Node(gr, {'type':'crypto', 'name':"Ethereum", 'iVaR':100.0})
Node(gr, {'type':'crypto', 'name':"Ethereum", 'iVaR':101.0})
Node(gr, {'type':'crypto', 'country':'France', 'name':"Ethereum", 'iVaR':110.0})
Node(gr, {'type':'crypto', 'country':'Germany', 'name':"Ethereum", 'iVaR':111.0})

Node(gr, {'type':'company', 'country':'Taiwan', 'name':'Canon', 'Stock':12.0, 'iVaR':24.0})
Node(gr, {'type':'company', 'country':'USA', 'name':'Apple', 'iVaR':14.0})
Node(gr, {'type':'company', 'country':'USA', 'name':'Google', 'iVaR':1.00004})
Node(gr, {'type':'company', 'iVaR':133.00004})