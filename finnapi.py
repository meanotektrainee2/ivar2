from typing import List, Dict, Union, Tuple, Callable, Optional
import requests
import time
import datetime
import csv
import traceback

API_key = ""

def set_api_key(key:str):
    '''присваивает API_key значение key'''
    global API_key
    API_key = key


def get_company_symbols(exchange: str="US")->List[Dict[str,str]]:
    '''API запрос, получает список сокращений компаний
        торгующихся на бирже и их описаний 
        https://finnhub.io/docs/api#stock-symbols

        результат запроса к /stock/symbol?exchange= exchange и
        заданным ключом API (API_key)
        методом GET
    '''
    newurl = "https://finnhub.io/api/v1/stock/symbol?exchange=" + exchange + "&token=" + API_key
    result = requests.get(newurl).json()
    list1 = [{'symbol':x['symbol'], 'description':x['description']} for x in result]
    return list1


def save_symbols(filename: str, basefunction: Callable, exchange: Optional[str]=None) -> None:
    '''* получает получает список сокращений c помощью функции basefunction
       * c использованием параметра exchange, если он не None,
       *  сохраняет данные в csv файл файл filename'''
    symbol_list = basefunction() if exchange is None else basefunction(exchange)
    fieldnames = ['symbol', 'description']
    with open(filename, mode='w') as csv_file:
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        csv_writer.writeheader()
        csv_writer.writerows(symbol_list)
    pass


def get_company_data(symbol: str)->Dict[str, Union[str,float]]:
    '''API запрос, получает описание компании по символу
        https://finnhub.io/docs/api#company-profile2

        результат запроса к /stock/profile2?symbol= symbol
        методом GET и с заданным ключом API (API_key)'''
    newurl = "https://finnhub.io/api/v1/stock/profile2?symbol="+ symbol + "&token=" + API_key
    result = requests.get(newurl).json()
    return result

def get_all_descriptions(filename: str, output_filename: str)->None:
    '''получить все описания компаний

        читает сsv файл filename со списком сокращений компаний
        для каждого сокращения получаете описание компании, записывает
        полученный словарь в csv файл output_filename по мере получения данных'''
        # нужно сделать паузу в 0.5 сек между запросами
    out_file = open(output_filename, "a")
    f_data = open(filename, "r")
    dataKeys = ['country','currency','exchange','finnhubIndustry','ipo','logo','marketCapitalization','name','phone','shareOutstanding','ticker','weburl']
    writer = csv.DictWriter(out_file, delimiter=',', fieldnames= dataKeys)
    reader = csv.DictReader(f_data, delimiter = ",")
    count = 0

    for row in reader:
        result = False
        errors = 0

        while not result:
            try:
                data = get_company_data(row['symbol'])
                result = True
            
                time.sleep(1)
                print(data)
                writer.writerow(data)
            
            except (ConnectionError, ConnectionAbortedError, ConnectionRefusedError, ConnectionResetError) as e:
                print('Connection error, try %d' % errors)
                time.sleep(10)
                errors += 1
    
    f_data.close()
    out_file.close()
    print('%d ConnectionErrors' % errors)

def get_forex_symbols(exchange: str = "oanda") -> List[Dict[str, str]]:
    '''API запрос, получает список сокращений компаний
        торгующихся на бирже и их описаний 
        https://finnhub.io/docs/api#forex-symbols

        результат запроса к /forex/symbol?exchange= exchange и
        заданным ключом API (API_key)
        методом GET
    '''
    url = 'https://finnhub.io/api/v1/forex/symbol?exchange=' + exchange + '&token=' + API_key
    data = requests.get(url).json()
    result = [{'symbol': element['symbol'], 'description': element['description']} for element in data]
    return result


def get_crypto_symbols(exchange: str="binance")->List[Dict[str,str]]:
    '''API запрос, получает список сокращений компаний
        торгующихся на бирже и их описаний 
        https://finnhub.io/docs/api#crypto-symbols

        результат запроса к /crypto/symbol?exchange= exchange и
        заданным ключом API (API_key)
        методом GET
    '''
    url = 'https://finnhub.io/api/v1/crypto/symbol?exchange=' + exchange + '&token=' + API_key
    data = requests.get(url).json()
    result = [{'symbol': element['symbol'], 'description': element['description']} for element in data]
    return result

        
def get_historical_price(date_from: float, date_to: float, symbol: str, asset_type: str) -> List[Tuple[float, float]]:
    '''получает исторические данные о стоимости акций компании symbol за период от date_from до date_to

    выполняет запрос к / asset_type /candle?symbol= symbol&resolution=D&from=date_from&to=date_to получает поле c,
    генерирует список timestamp всех дат от date_from до date_to c шагом в сутки, объединяет эти два списка

    Returns:
    (float - timestamp
    float - цена закрытия)
    '''
    start_url = 'https://finnhub.io/api/v1/' + asset_type + '/candle?symbol=' + symbol
    end_url = '&resolution=D&from=' + str(int(date_from)) + '&to=' + str(int(date_to)) + '&token=' + API_key
    url = start_url + end_url
    data = requests.get(url).json()
    cost_list = data["c"]
    timestamp_list = [float(date) for date in range(int(date_from),  int(date_to + 1.0), 86400)]
    result = list(zip(timestamp_list, cost_list))
    return result


def save_historical_prices(date_from: str, date_to: str, symbol: str, asset_type: str):
    '''сохраняет данные об истории изменения цен актива symbol типа asset_type в csv файл с именем asset_name_date_from_date_to.csv

    преобразовывает date_from и date_to из строки в timestamp, получает исторические данные о стоимости акций (get_historical_price()),
    сохраняет полученные данные в csv файл c заголовками date и price'''

    file_name = symbol + str(date_from) + str(date_to) + '.csv'
    start_date = time.mktime(datetime.datetime.strptime(date_from, "%m-%d-%Y").timetuple())
    end_date = time.mktime(datetime.datetime.strptime(date_to, "%m-%d-%Y").timetuple())
    data = get_historical_price(start_date, end_date, symbol, asset_type)
    with open(file_name, mode='w') as csv_file:
        fieldnames = ['date', 'price']
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        csv_writer.writeheader()
        for row in data:
            csv_writer.writerow({'date': row[0], 'price': row[1]})
    pass

def save_prices_for_all(filename: str, active_type: str):
    errors = 0
    with open(filename, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            symbol = row['symbol']
            try:
                save_historical_prices('01-01-2017', '08-01-2020', symbol, active_type)
                time.sleep(1.25)
            except:
                print(symbol)
                errors +=1
    print('Saved with %d errors' % errors)

if __name__=="__main__":
    set_api_key('bsqfmu748v6p29vi1010')
    #получить все символы компаний, сохранить в файл, затем получить и сохранить все описания компаний
    #print(get_company_symbols())
    #save_symbols('companies.csv', get_company_symbols)
    #print(get_company_data('ACB'))
    #get_all_descriptions('companies.csv', 'new_data.csv')
    #print(get_forex_symbols())
    #print(get_crypto_symbols())
    #print(get_historical_price(1483228800, 1597827375, 'BINANCE:XTZBNB', 'crypto'))
    #print(get_historical_price(1483228800, 1597827375, 'BINANCE:CMTETH', 'crypto'))
    save_historical_prices('01-01-2017', '08-19-2020', 'BINANCE:LINKBUSD', 'crypto')
    #save_prices_for_all('crypto.csv', 'crypto')
    #save_prices_for_all('forex.csv', 'forex')