import neuthink.graph.basics as bs
from typing import List, Dict, Optional, Union
import get_ivar_for_half as gi
import csv, re, os


def get_filename(files: List[str], name: str) -> Optional[str]:
    '''В списке файлов files находит тот, что совпадает с указанным name
    None, если такого файла не нашлось'''
    for x in files:
        a = re.findall(r'.+(?=.{24})', x)
        for y in a:
            if y == name:
                return x
    return None


def full_data_search(full_data: str, name: str) -> Dict[str, str]:
    '''
    В файле full_data, содержащем все данные о компаниях, находит ticker равный name.
    Находит и возвращает значения name и country по этому тикеру.
    '''
    newdata = {}
    keys = ['country','currency','exchange','finnhubIndustry','ipo','logo','marketCapitalization','name','phone','shareOutstanding','ticker','weburl']
    with open(full_data,'r') as datafile:
        fulldatareader = csv.DictReader(datafile, fieldnames=keys)
        for x in fulldatareader:
            if x['ticker'] == name:
                newdata = {'company_name':x['name'], 'country':x['country']}
        return newdata


def get_data(name:str, active_type: str, filename:str, full_data_file:str = '') -> Dict[str, str]:
    '''
    Возвращает словарь, содержащий подсчитанный iVaR, данные о типе, стране и названии.
    '''
    data = {}
    if active_type == 'company':
        data = full_data_search(full_data_file, name)
    data['type'] = active_type
    data['name'] = name
    return data


def create_database(filein: str, files: List[str], active_type: str, path:str, full_data_file: str = '', outfilename: str = 'ivar_all.csv') -> None:
    '''
    Читает файл filein. Для каждого символа из filein получает словарь с данными из get_data.
    Записывает полученный словарь в файл outfilename.
    '''
    outfile = open(outfilename, 'a')
    csvin = open(filein, 'r')
    writer = csv.DictWriter(outfile, fieldnames = ['name', 'country', 'company_name', 'iVaR', 'type'])
    reader = csv.DictReader(csvin, fieldnames=['symbol', 'description'])

    for x in reader:
        name = x['symbol']
        filename = get_filename(files, name)

        if isinstance(filename, str):
            newfile = path + filename
            how_long = gi.get_coords(newfile,1530396000.0)
            ivar = gi.count_ivar(newfile,1530396000.0)
            if len(how_long) > 49 and isinstance(ivar, float):
                if active_type == 'company':
                    data = get_data(name, active_type, newfile, full_data_file)
                else:
                    data = get_data(name, active_type, newfile)
                data['iVaR'] = str(ivar)
                print(data)
                writer.writerow(data)
    
    outfile.close()


if __name__ == "__main__":
    outfile = open('ivar_half.csv', 'a')
    writer = csv.DictWriter(outfile, fieldnames = ['name', 'country', 'company_name', 'iVaR', 'type'])
    writer.writeheader()
    outfile.close()

    ###CRYPTO###
    crypto_path = '/home/railya/ivar/crypto/'
    crypto_file = '/home/railya/ivar/crypto_all.csv'
    crypto_files = os.listdir(path=crypto_path)
    create_database(crypto_file, crypto_files, 'crypto', crypto_path)

    ###FOREX###
    forex_path = '/home/railya/ivar/forex/'
    forex_files = os.listdir(forex_path)
    forex_file = '/home/railya/ivar/forex_all.csv'
    create_database(forex_file, forex_files, 'forex', forex_path)
    
    ###COMPANY###
    company_path = '/home/railya/ivar/company/'
    company_file = '/home/railya/ivar/companies_all.csv'
    company_full = '/home/railya/ivar/companies_full_data.csv'
    company_files = os.listdir(company_path)
    create_database(company_file, company_files, 'company', company_path, company_full)