'''Модуль сохраняет правильные csv файлы для 1.5 и 3 лет'''
from typing import List, Dict, Union, Tuple, Callable, Optional
import countbacktest as cnt
import csvdatacollect as collectd
from typing import *
import neuthink.graph.basics as bs
import csv
from neuthink.graph.basics import Graph, Node
from neuthink import metatext as txt


def open_and_filter_ivar(actives: List[str], inscvfile: str = 'ivar_half.csv')-> bs.NodeList:
    '''
    Записать новый csv с нужными датами и активами

    1)Необходимо преобразовать outcsvfile в граф
    cb.csv_into_nodelist(incsvfile), где incsvfile - входящий в функцию аргумент

    2)Перебрать все значения по ключу 'name' , если значния  нет в списке actives - пропустить, 
    если есть - добавить в пустой список relev_ivar

    3) Вернуть relev_ivar 
    '''

    gr = txt.LoadCSV(inscvfile).parent_graph
    nodes = gr.Match({})
    print(nodes)
    relev_ivar = [node for node in nodes if node.get('name') in actives]
    return relev_ivar


def check_csv(actives: List[str], relev_ivar: bs.NodeList) -> bool:
    '''
    Проверка на количество элементов в эталонном списке, и в спсике узлов

    Необходимо проверить равно ли количество элементов в actives с количсетвом жлементов в relev_ivar
    Если совпадает -> True
    Иначе -> False
    '''
    is_same_element_numbers = len(actives) == len(relev_ivar)
    return is_same_element_numbers


def make_ivar(relev_ivar: bs.NodeList, outcsvfile: str = 'ivar_15.csv')-> None:
    '''
    Создает файл csv с ivar

    Необходимо записать в новый файл-csv  с названием  outcsvfile список узлов relev_ivar
    Например
    >>> relev_ivar = [{'name': 'lase', 'ivar': '21.21', 'country': 'USA'},{'name': 'HTC', 'ivar': '121.21', 'country': 'China'}, ...]
    outcsvfile
    name  ivar      country
    lase  21.21     USA
    HTC   121.21    China 
    '''
    with open(outcsvfile, mode='w') as csv_file:
        field_names = (relev_ivar[0].keys())
        csv_writer = csv.DictWriter(csv_file, fieldnames=field_names)
        csv_writer.writeheader()
        for row in relev_ivar:
            csv_writer.writerow(row)
        



if __name__ == "__main__":
    actives = collectd.txt_into_list('actives.csv')

    relev_ivar = open_and_filter_ivar(actives)
    make_ivar(relev_ivar)
    check_csv(actives, relev_ivar)