import traceback, logging
import tornado
import tornado.ioloop
import tornado.web
import os, json
import actives_search as acs
import countbacktest as cbt

class StartHandler(tornado.web.RequestHandler):
    def post(self):
        payload = json.loads(self.request.body.decode('utf-8'))
        try:
            res_ivar = acs.search_actives(payload[0], num=payload[1],what_ivar = payload[3])
            result = cbt.return_backtest(payload[4],payload[2],res_ivar)
        except:
            result = {"status": "error"}
            logging.log(logging.ERROR, traceback.format_exc())
        self.write(json.dumps(result, ensure_ascii=False).encode('utf8'))
        

settings = dict()
application = tornado.web.Application([
    (r"/", StartHandler)
],
    template_path=os.path.join(os.path.dirname(__file__), "templates"),
    static_path=os.path.join(os.path.dirname(__file__), "static"),
    autoreload=True,
    debug=True,
    **settings
)

if __name__ == "__main__":
     try:
        f = open("config.json", "r")
        # ~ a = f.read()
        config = json.load(f)
        server_ip = config["server-ip"]
        server_port = config["server-port"]
        protocol = 'http://'
        host_url = protocol + server_ip + ":" + server_port + "/"
        print("start with:     " + host_url)

     except:
         print("Can not load file config.json")
         traceback.print_exc()
     else:

        print("starting server...")

        http_server = tornado.httpserver.HTTPServer(application)
        http_server.listen(server_port)


        tornado.ioloop.IOLoop.instance().start()