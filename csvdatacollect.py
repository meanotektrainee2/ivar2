'''Модуль для сборки большого csv файла'''
from typing import List, Dict, Union, Tuple, Callable, Optional
import csv
import datetime
import time
import countbacktest as cnt
import os



def extract_name(filename: str)-> str:
    '''
    Получает название актива из названия файла

    Необходимо 'обрезать' строку с начала до первой найденной цифры
    >>> extract_name('OANDA:EUR_GBP01-01-201708-01-2020.csv')
    'OANDA:EUR_GBP'
    '''
    digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    name = ''
    for sign in filename:
        if sign in digits:
            return name
        name += sign
    return name


def gauge(filename: str) -> Dict[str, Dict[str, str]]:
    '''
    Читает csv-файл и заносит данные в словарь из словарей

    Необходимо прочитать csv-файл , например с названием: 'OANDA:EUR_GBP01-01-201708-01-2020.csv'
    Сформировать словарь следующего вида:
    {date:{name:price},date:{name:price} ... },где
    date - первый столбик в csv файле, price - второй столбик , а name - название файла до первой цифры , в нашем случае  'OANDA:EUR_GBP' с помощью функции extract_name(filename)
    в итоге должно получится:
    {'1483218000':{'OANDA:EUR_GBP':'0.84994'}, '1483304400': {'OANDA:EUR_GBP':'0.85095'}, .......}
    '''
    name = extract_name(filename)
    with open(filename) as csv_file:
        data = csv.DictReader(csv_file)
        result_dict = {change_date(row['date']): {name: row['price']} for row in data}
    return result_dict


def act_in_list(except_act: List[str], filename: str) -> None:
    '''
    Занесение названия актива в список

    На вход приходит filename, вытаскиваем из него название с помощью функции extract_name(filename)
    Добавляем в список название файла
    >>> l = []
    >>> act_in_list(l,'OANDA:EUR_GBP01-01-201708-01-2020.csv')
    >>> l
    ['OANDA:EUR_GBP']
    >>> act_in_list(l,'AAL01-01-201701-01-2020.csv')
    >>> l
    ['OANDA:EUR_GBP', 'AAL']
    '''
    name = extract_name(filename)
    except_act.append(name)
    pass







def check_dates(first_date: str, last_date: str) -> bool:
    '''
    Проверяет first_date и last_date на соответсвие

    Необходимо перевести first_date и last_date в формат 'int',затем
    1) Сравнить first_date и число 1483221600 , если first_date <= 1483480800, то переходим ко второму пункту ,иначе возвращаем False и к bad_start прибавляем 1

    2) Сравнить last_date и число 1577829600, если  last_date >= 1577829600, то возвращаем True, иначе возвращаем False и к bad_end прибавляем 1
    >>> bad_start = 0
    >>> bad_end = 0
    >>> check_dates( 100, 1000 )
    False
    >>> bad_start
    0
    >>> bad_end 
    1
    '''
    global bad_start, bad_end
    if float(first_date) > cnt.from_date_to_timestamp('03/01/2017'):
        bad_start += 1
        return False
    if float(last_date) < cnt.from_date_to_timestamp('31/12/2019'):
        bad_end += 1
        return False
    return True


def act_to_dict(filename: str, all_actives: List[List[str]], temple_dict:Dict[str,Dict[str,str]]) -> Dict[str,Dict[str,str]]:
    '''
    Функция записи в словарь temple_dict значений all_actives и filename

    Получаем значение функции extract_name(filename), где filename - входной параметр 
    Результат функции называем name
    
    all_actives - список из пар значений [[date, price], [date, price], ...]
    temple_dict - словарь вида {date:{name:price},date:{name:price} ... }
    Записываем в словарь temple_dict значения all_actives следующим способом:
    для каждого списка в all_actives находим в словаре temple_dict словарь с такой же date и добавляем в значение по этому ключу name, который нашли ранее и price из пары в списке вида: 'name:price',
    если такой date нет , то пропускаем и переходим к следующей паре
    >>> all_actives =  [['10410', '77.8807951214426'], ['10413', '131.0011152916978'],['10416', '69.30161426939232']]
    >>> temple_dict = {'10410':{'OANDA:EUR_GBP':'0.84994'}, '10416': {'OANDA:EUR_GBP':'0.85095'}}
    >>> temple_dict = act_to_dict('AAP01-01-201701-02-20.csv', all_actives, temple_dict)
    >>> temple_dict
    {'10410':{'OANDA:EUR_GBP':'0.84994', 'AAP':'77.8807951214426'}, '10416': {'OANDA:EUR_GBP':'0.85095', 'AAP': '69.30161426939232'}}
    '''
    name = extract_name(filename)
    for active in all_actives:
        date, price =  active[0], active[1]
        if date in temple_dict:
            temple_dict[date][name] = price
    return temple_dict







def check_csv(filename: str, except_act: List[str],temple_dict:Dict[str,Dict[str,str]] ) -> None:
    """
    Открывает csv файл и проверяет первую и последнюю дату и записывает в словарь

    Необходимо открыть файл filename, найти в нем первую и последнюю дату(даты в csv файле упорядочены)
    и проверить их(первую и последнюю дату) с помощью функции check_dates(first_date, last_date), 
    где first_date, last_date - первая и последняя дата, а bad_start
    Если функция возвращает True, то добавляем файл в спискок except_act с помощью функции act_in_list(except_act,filename),
    а также передаем лист со значениями активов в функцию act_to_dict(filename, all_actives), где filename - название файла, а all_actives - список из списков со значениями - [date,price] без заголовка
    вида: [['10410', '77.8807951214426'], ['10413', '131.0011152916978'], ['10416', '69.30161426939232'], ['10419', '52.11662971963613']...]
    """
    with open('folder_all/'+filename) as csv_file:
        data = [row for row in csv.DictReader(csv_file)]
        first_date, last_date = data[0]['date'], data[-1]['date']
        print(filename, first_date, last_date,check_dates(first_date, last_date))
        if check_dates(first_date, last_date) and len(data)>730:
            print(len(data), 'LEN DATA', filename)
            act_in_list(except_act, filename)
            all_actives = [[change_date(element['date']), element['price']] for element in data]
            temple_dict = act_to_dict(filename, all_actives,temple_dict)

        else:
            pass
    pass



def read_csvfile_in_folder(foldername: str,except_act:List[str], temple_dict:Dict[str,Dict[str,str]]) -> None:
    '''
    Читает все файлы в папке

    Необходимо перебрать все файлы в папке и передавать их в качестве входных данных функции check_csv(filename,except_act) пока они не закончатся,
    а также список except_act
    В конце прочтения напечатать результаты списков с надписью:
    для bad_start - "НЕ ПОДОШЛИ ИЗ-ЗА НАЧАЛЬНОЙ ДАТЫ"
    для bad_end  - "НЕ ПОДОШЛИ ИЗ-ЗА КОНЕЧНОЙ ДАТЫ"
    '''
    csv_files = [file_name for file_name in os.listdir(foldername) if file_name.endswith('.csv')]
    for fille in csv_files:
        check_csv(fille,except_act,temple_dict)
    pass


def filterTheDict(dictObj, callback):
    newDict = dict()
    # Iterate over all the items in dictionary
    for (key, value) in dictObj.items():
        # Check if item satisfies the given condition then add to new dict
        if callback((key, value)):
            newDict[key] = value
    return newDict





def sort_and_filter(temple_list: List[Dict[str, str]], rel_actives: List[str]) -> Tuple[List[Dict[str,str]], List[str]]:
    '''
    Сортируем и фильтруем список

    На вход поступает список temple_list вида:[{'date': '1483218111','EUR': '60', 'RUB': '10'}, { 'date': '1483217000', 'EUR' : '61', 'RUB': '10', 'GOLD': '43'}, { 'date': '1577826000', 'EUR' : '61', 'RUB': '10', 'GOLD': '33'} ...]
    И список rel_actives вида: ['EUR','RUB','GOLD']
    Необходимо отсорировать список в порядке возрастания по ключу 'date'
    Создать новый спискок и включать в него только те словари, значение по 'date' у которых больше или равно 1483221600 и меньше или равно 1577829600, а также
    у которых есть все ключи, которые находятся в rel_actives:
    >>> temple_list = [{'date': '1483218111','EUR': '60', 'RUB': '10'}, { 'date': '1483217000', 'EUR' : '61', 'RUB': '10', 'GOLD': '43'}, { 'date': '1577826000', 'EUR' : '61', 'RUB': '10', 'GOLD': '33'}]
    >>> rel_actives = ['EUR','RUB','GOLD']
    >>> sort_and_filter(temple_list)
    [{ 'date': '1577826000', 'EUR' : '61', 'RUB': '10', 'GOLD': '33'}]
    '''

    result_list = []
    
    sort_by_date = lambda element: element['date']
    sorted_list = sorted(temple_list, key=sort_by_date)
    rel_actives1 = rel_actives.copy()
    for element in sorted_list:
        key_list = [key for key in element if key != 'date']
        if set(key_list) != set(rel_actives1):
            print(element['date'])
            print(cnt.timestamp_to_date(float(element['date'])))
            print(set(rel_actives1) - set(key_list)) 
            print(set(key_list) - set(rel_actives1), 'FROM key_list' )
        else:
            print(cnt.timestamp_to_date(float(element['date'])), 'GOT IN') 
            
            

            
        if cnt.from_date_to_timestamp('01/01/2017') <= float(element['date']) <= cnt.from_date_to_timestamp('01/01/2020') and len(element) > 5100:
            result_list.append(element)
            rel_actives = list(set(rel_actives).intersection(set(element.keys())))
            print(len(rel_actives),'HOW MANY ACTIVES AS ETALON')
       
    new_res = [filterTheDict(element, lambda elem : elem[0] in rel_actives or elem[0] == 'date') for element in result_list]
    print(len(rel_actives), '<-- ОСТАЛОСЬ АКТИВОВ ПОСЛЕ ФИЛЬТАЦИИ')
    return new_res, rel_actives
    




def from_dict_to_list(temple_dict:Dict[str,Dict[str,str]],rel_actives: List[str]) -> List[Dict[str,str]]:
    '''
    Превращаем словарь словарей в список словарей,сортируем его

    Необходимо изменить словарь словарей вида {date:{name:price, name1:price1},date:{name2 :price2, name3: price3} ... } так,чтобы объединить ключ и значения в один словарь, то есть убрать одну вложенность
    На выходе должно получится:
    temple_list = [{'datе': date, name:price, name4:price4}, {'date': date, name2:price2, name3:price3} ...]
    Более понятный пример со значениями:
    Исходный словарь словарей:  {'10410':{'EUR': '60', 'RUB': '10'}, '10411':{ 'EUR' : '61', 'RUB': '10'} ... } 
    Должно вернуться: [{'date': '10410','EUR': '60', 'RUB': '10'}, { 'date': '10411', 'EUR' : '61', 'RUB': '10'} ...]
    После этого передать полученный список в качестве аргумента функции sort_and_filter_dict(temple_list), где temple_list - полученный список
    Вернуть результат функции sort_and_filter_dict
    >>> temple_dict = {'1483218111':{'EUR': '60', 'RUB': '10'}, '1483217000': {'EUR' : '61', 'RUB': '10', 'GOLD': '43'}, '1577826000':{ 'EUR' : '61', 'RUB': '10', 'GOLD': '33'}}
    >>> from_dict_to_list(temple_dict)
    [{'date': '1577826000', 'EUR': '61', 'RUB': '10', 'GOLD': '33'}]
    '''
    temple_list = [{'date': key, **val} for key, val in temple_dict.items()]
    return sort_and_filter(temple_list, rel_actives)




def change_date(timestamp: str) -> str:
    """
    Функция берет время и дату и приводит дату к одному времени 00:00

    Сначала необходимо привести формат datestamp к формату date, например:
    >>> timestamp = 1483218111
    >>> print(datetime.datetime.fromtimestamp(timestamp))
    2016-12-31 23:01:51

    После этого необходимо взять дату в формате 31/12/2016 и перевести в формат datestamp c
    помощью cnt.from_date_to_timestamp(date), где date = 31/12/2016
    >>> date = 31/12/2016
    >>> cnt.from_date_to_timestamp(date)
    '1483135200.0'
    """
    date = cnt.timestamp_to_date(float(timestamp))
    datestamp = cnt.from_date_to_timestamp(date)
    return str(datestamp)




def from_list_to_csv(sorted_list:List[Dict[str,str]] , out_csv_name:str) -> None:
    '''
    Записываем список в csv файл

    На вход поступает список sorted_list вида
    [{ 'date': '1577826000', 'EUR' : '61', 'RUB': '10', 'GOLD': '33'},{ 'date': '1609365600', 'EUR' : '62', 'RUB': '11', 'GOLD': '34'}]
    создается файл csv с названием out_csv_name, в котором
    первая строка ключи словарей(они у всех одинаковые),
    последующие строки - значения словарей,
    то есть вторая строка csv - файла значения первого словаря, а третья строка -значения второго словаря
    необходимо преобразовать значения по ключу date с помощью функции change_date(timestamp), где timestamp равен значению по ключу date

    должно получиться так:
    date            EUR     RUB     GOLD
    1577743200      61      10      33
    1609365600      62      11      34
    '''
    with open(out_csv_name, 'w') as f:
        fieldnames = (sorted_list[0].keys())
        csv_writer = csv.DictWriter(f, fieldnames=fieldnames)
        csv_writer.writeheader()
        for row in sorted_list:
            csv_writer.writerow(row)



def list_into_txt(rel_actives: List[str], outfilename: str) -> None:
    '''
    Список сохранить в csv файл

    Необходимо сохранить список rel_actives в csv файл outfilename
    '''
    with open(outfilename, 'w') as f:
        csv_writer = csv.writer(f,delimiter=',')
        csv_writer.writerow(rel_actives)




def txt_into_list(infilename: str) -> List[str]:
    '''
    Вытащить из csv файла список

    Из файла infilename сделать список
    Например:
    Book
    Pen
    Door

    Возвращается: ['Book', 'Pen', 'Door']
    '''
    result_list:List[str] = []
    with open(infilename, 'r') as inf:
        reader = csv.reader(inf)
        for elem in reader:
            result_list[len(result_list):] += elem
    return result_list






if __name__ == '__main__':
    except_act = []
    bad_start = 0
    bad_end = 0
    temple_dict = gauge('FVAL01-01-201701-01-2020.csv')
    path = 'folder_all'
    read_csvfile_in_folder(path, except_act, temple_dict)
    temple_list, except_act = from_dict_to_list(temple_dict,except_act)[0], from_dict_to_list(temple_dict,except_act)[1]
    print(len(temple_list[0])-1, len(except_act),'actives in csv' )
    from_list_to_csv( temple_list, 'big_csv_data.csv')
    list_into_txt(except_act, 'except_act.csv')
    print(bad_start, bad_end,'trash')


