from typing import Union, Dict, List, Tuple, Callable, Optional
import neuthink.graph.basics as bs
import csv

def fall_area(max_coord: List[Tuple[int, float]]) -> float:
    '''
    Считает полную площадь между всеми координатами.
    
    Длина - разница между первыми элементами обоих кортежей разделённая на 86400 + 1.0
    Ширина - значение второго элемента последнего кортежа
    '''
    width = (max_coord[-1][0] - max_coord[0][0]) / 86400 + 1.0
    if max_coord[0][1] >= max_coord[1][1]:
        length = max_coord[0][1]
    else:
        length = max_coord[1][1]
    return length*width

def full_area(max_coord) -> float:
    '''
    Считает площадь между двумя координатами
    '''
    width = (max_coord[-1][0]- max_coord[0][0])/86400
    if max_coord[0][1] >= max_coord[1][1]:
        length = max_coord[0][1]
    else:
        length = max_coord[1][1]
    return length*width



def count_area(timeline: List[int], coords: List[Tuple[int, float]]) -> float:
    '''
    Посчитать сумму площадей для каждого участка на timeline.

    Необходимо пройтись по всему списку кортежей coords, сложить все полученные площади
    Если timeline не равна первому элементу в coords, тогда брать число из timeline, ближайшее к coords.
    Разница между координатами равна abs(coord - timeline)
    Площадь = разница между координатами умноженная на значение второго элемента кортежа.
    '''
    minimum = lambda date,coords : min([abs(coords[i][0]-date) for i in range(len(coords))])
    hole : Callable[...,Tuple[int, float]] =  lambda date,coords,i = 0 : (date,coords[i][1]) if abs(coords[i][0]-date) == minimum(date,coords) else hole(date,coords,i+1) 
    filled = [hole(date,coords) for date in timeline]
    full_area_sum = sum([full_area([filled[i], filled[i+1]]) for i in range(len(filled)-1)])
    return full_area_sum

def get_timeline(coords: List[Tuple[int, float]]) -> List[int]:
    '''
    Получает список дат с шагом 1 день(86400 секунды).

    Вернуть список, содержащий точки с шагом 86400. Края списка - элементы из coords

    >>> get_timeline([('5270', '119.08'), ('5279', '135.676')])
    [5270, 5271, 5272, 5273, 5274, 5275, 5276, 5277, 5278, 5279]
    '''
    max_start, max_end = coords[0][0], coords[-1][0]
    return [my_tup for  my_tup in range(max_start, max_end+1, 86400)]



def get_coords_time(filename: str, time: float) -> List[Tuple[int, float]]:
    '''
    Берёт ivar в файле filename за время time

    Читает файл filename, возвращает список кортежей. Кортежи формируются из полей date и price.
    '''
    with open(filename) as f:
        reader = list(csv.reader(f))
        all_time =  list(map(lambda x: (int(float(x[0])), float(x[1])) , reader[1:]))
        half_of_the_time = list(filter(lambda x: x[0] <=  time, all_time))
        return half_of_the_time


def get_coords(filename: str) -> List[Tuple[int, float]]:
    '''
    Получает координаты всех точек из файла filename.

    Читает файл filename, возвращает список кортежей. Кортежи формируются из полей date и price.
    '''
    with open(filename) as f:
        reader = list(csv.reader(f))
        return list(map(lambda x: (int(float(x[0])), float(x[1])) , reader[1:]))

def len_hole(ivar, blackhole):
    '''
    Проверяет количество координат в blackhole.

    Если на выбранном участке 2 точки - это точка роста.
    Иначе - считает и возвращает значение ivar
    '''
    return ivar + fall_area(blackhole) - count_area(get_timeline(blackhole), blackhole) if len(blackhole) > 2 else ivar

def count_ivar(coords: List[Tuple[int, float]]) -> Optional[float]:
    '''
    Находит точку, после которой идёт падение, ищет точку восстановления, считает ivar между ними.
    '''
    ivar = 0.0

    blackhole = []
    maxv = coords[0][1]
    for i in range(0, len(coords)):
        blackhole.append(coords[i])

        if i < len(coords)-1 and coords[i+1][1] >= maxv:
            blackhole.append(coords[i+1])
            maxv = coords[i+1][1]
            ivar = len_hole(ivar, blackhole)
            blackhole = []
        
    ivar = len_hole(ivar, blackhole)
    return ivar


if __name__ == "__main__":
    a = [(172812, 3.0), (259212, 2.0), (345612, 1.0), (432012, 2.0), (518412, 3.0)]
    #print(count_area(timeline, a))
    #print(count_ivar(get_coords_time('/home/user/projects/ivar/company/MID01-01-201701-01-2020.csv', 1530396000.0)))