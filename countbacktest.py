'''Подсчитывает backtest '''
import neuthink.graph.basics as bs
#from neuthink import metatext as txt
from typing import List, Dict, Union, Tuple, Callable, Optional
import time
import datetime
from neuthink.graph.basics import Graph, Node

import plotly.graph_objects as plot
import uuid

#gr = txt.LoadCSV('big_csv_data.csv').parent_graph
# gr = bs.Graph()

from dateutil import tz
def from_date_to_timestamp(usdate:str)->float:
    '''
    Функция переводит дату в формат datestamp
    
    >>> from_date_to_timestamp('31/12/2016')
    1483131600.0
    '''
    dt = datetime.strptime(usdate, "%d/%m/%Y")
    timestamp = dt.replace(tzinfo=timezone.utc).timestamp()
    return timestamp
from_date_to_timestamp('31/12/2016')




def find_the_first(date:float) -> float:
    '''
    Находит дату если отсутсвует текущая начальная дата

    Проверяет пустой ли узел c 'date': date , если пустой то к date прибавляют 86400
    до тех пор пока узел не перестанет быть пустым
    !!! date входящий в формате float, чтобы проверить на пустоту в графе необходио преобразовать в str!!!
    иначе возвращает ту же дату
    >>> gr = bs.Graph()
    >>> Node(gr, {'country':'Russia', 'name':"Ruble", 'date':'0.0'})
    >>> Node(gr, {'country':'Russia', 'name':"Ruble", 'date': '172800.0'})
    >>> Node(gr, {'country':'China', 'name':"Yuan", 'date':'259200.0'})
    >>> Node(gr, {country':'China', 'name':"Dollar", 'date':'432000.0'})
    >>> find_the_first(86400.0)
    172800
    '''
    while  gr.MatchOne({'date': str(date)}).get('type') == 'empty':
        date += 86400
    return date



def find_the_last(date: float) -> float:
    '''
    Находит дату если отсутсвует текущая последняя дата

    Проверяет пустой ли узел c 'date': date , если пустой то от date отнимают 86400
    до тех пор пока узел не перестанет быть пустым
    !!! date входящий в формате float, чтобы проверить на пустоту в графе необходио преобразовать в str!!!
    иначе возвращает ту же дату
    >>> gr = bs.Graph()
    >>> Node(gr, {'type':'forex', 'country':'Russia', 'name':"Ruble", 'date':'0.0'})
    >>> Node(gr, {'type':'forex', 'country':'Russia', 'name':"Ruble", 'date': '172800.0'})
    >>> Node(gr, {'type':'forex', 'country':'China', 'name':"Yuan", 'date':'259200.0'})
    >>> Node(gr, {'type':'forex', 'country':'China', 'name':"Dollar", 'date':'432000.0'})
    >>> find_the_last(518400.0)
    432000.0
    '''
    while gr.MatchOne({'date': str(date)}).get('type') == 'empty':
        date -= 86400
    return date



def datestamp(period_for_back: Tuple[str,str]) -> Tuple[float,float]:
    '''
    Переводит кортеж с началом и концом даты в формат datestump

    На вход приходит кортеж вида : ('01/01/2017','01/01/2020')
    Нeобходимо перевести его в формат timestamp
    Затем проверить каждый элемент на предмет существования в графе,
    необходимо вызвать функцию find_the_first(date) для первой даты,
    и функцию для find_the_last(date)  для второй даты,
    где date - дата в формате timestamp
    Результат вернуть в виде кортежа
    >>> per =('01/01/2017','01/01/2020')
    >>> gr = bs.Graph()
    >>> Node(gr, {'type':'forex', 'country':'Russia', 'name':"Ruble", 'date': '1546293600.0'})
    >>> Node(gr, {'type':'forex', 'country':'China', 'name':"Yuan", 'date':'1483221600.0'})
    >>> Node(gr, {'type':'forex', 'country':'China', 'name':"Dollar", 'date':'0.0'})
    >>> datestamp(per)
    (1483221600.0, 1577829600.0)

    '''
    strt1, end1 = from_date_to_timestamp(period_for_back[0]), from_date_to_timestamp(period_for_back[1])    
    print(find_the_first(strt1), find_the_last(end1), 'TTIIIIIIMMMMMEEE')
    return find_the_first(strt1), find_the_last(end1)
# per =('01/01/2017','01/01/2020')
global gr
def just():
    global gr

    gr = bs.Graph()
    Node(gr, { 'country':'Russia', 'name':"Ruble", 'date': '1577221200.0'})
    Node(gr, {'country':'China', 'name':"Yuan", 'date':'1485291600.0'})
    Node(gr, { 'country':'China', 'name':"Dollar", 'date':'0.0'})
    print(gr.Match({}))

just()
print(gr.Match({}))


# def csv_into_nodelist(csvfilename: str) -> None:
#     '''
#     Читает большой csv файл и преобразует его в NodeList
#     '''
    
#     pass



def get_asset(result: Dict[str,List[Dict[str,str]]]) -> Dict[str,float]:
    '''
    Получает список активов с процентами активов в портфеле

    Получает на вход словарь вида: {'forex':[{'name': 'EUR', 'type':'','country':''},{'name': 'RUB', 'type':'','country':''}],'crypto': [{'name': 'buxcoin', 'type':'','country':''}], 'company':[]}
    Необходимо преобразовать словарь в словарь вида {'EUR':0.34,'RUB':0.33, 'buxcoin':0.33}
    Для этого необходимо пройтись по значениям ключей 'forex', 'crypto','company'
    Каждое значение представляет собой список словарей,
    из каждого словаря необходимо извлечь значение ключа 'name' и
    процент из общего количества значений-списков ключей
    Процент должен быть  в виде десятичного значения и округен по правилам математики
    Если сумма не равна 1, то к первому активу прибавить 1% или отнять 1%
    Необходимо иметь ввиду, что списки-значения могут быть пустыми как в примере с коючом 'company'
    >>> result = {'forex':[{'name': 'EUR', 'type':'','country':''},{'name': 'RUB', 'type':'','country':''}],'crypto': [{'name': 'buxcoin', 'type':'','country':''}], 'company':[]}
    >>> get_asset(result)
    {'EUR':0.34,'RUB':0.33,'buxcoin':0.33}
    '''
    list_for_count = [x['name'] for key in result for x in result[key]]
    port_of_actives = {active: round(1/len(list_for_count), 2) for active in list_for_count}
    # summ_of_values_poa = sum(port_of_actives.values())
    # delta = 1 - summ_of_values_poa
    # if summ_of_values_poa < 1:
    #     port_of_actives['EUR'] += delta
    # else:
    #     port_of_actives['EUR'] -= delta
    print(port_of_actives)
    return port_of_actives


def price_for_date(date:str ,active:str)-> int:
    '''
    Определяет цену на определенный день у определенного актива

    gr вида: [{'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22',......},{'date':'157790000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24',......}...]
    необходимо найти узел 'date': date и вывести значение по ключу active
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'157790000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> price_for_date('1577892600.0', 'RUB')
    10
    '''
    find_active = gr.MatchOne({'date':date})[active]
    return int(find_active)



def amount_of_actives(date:float, invested_sum: float, asset: Dict[str,float]) -> Dict[str,int]:
    '''
    Получает количество акций каждого актива

    На вход поступает date вида 1577829600.0
    invested_sum вида 10000.0
    asset вида {'EUR':0.34,'RUB':0.33, 'buxcoin':0.33}
    Необходимо сформировать словарь, где ключи остнутся прежними,
    а значения будут равны значению amount
    Amount для каждой пары равен:
    invested_sum , умноженный на значение по ключу пары и разделенный на price_for_date
    price_for_date для каждой пары равен результату функции  price_for_date(date,active), где date равен входному параметру, а active - ключу пары
    Значение amount округлять в меньшую сторону
    Числовой пример:
    Рассчитаем amount для EUR
    date = 157790000.0
    price_for_date = 63
    amount = 10000.0 * 0.33 / 63 = 52.38 = 52
    Функция вовращает словарь вида:{'EUR':52,'RUB':30,....}
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'157790000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> amount_of_actives('1577892600.0', 10000.0, {'EUR':0.34,'RUB':0.33, 'TIEN':0.33})
    {'EUR': 56,'RUB': 330, 'TIEN': 150}
    
    '''
    
    amount = {key:int(invested_sum * asset[key]/price_for_date(str(date),key)) for key in asset}
    print(amount, 'AMOUNT')
    return amount


def count_act_for_one(amount_of_actives: Dict[str,int],curr_date: float)-> int:
    '''
    Посчитать стоимость портфеля на определенную дату

    На вход придет amount_of_actives вида {'EUR':52,'RUB':30,....}
    curr_date: 1483826400.0
    необходимо для каждого ключа словаря amount_of_actives рассчитать следующeе значение:
    умножение значения по этому ключу на значение функции price_for_date(date,active),
    где active- ключ по этому значению, например 'EUR', а date равен curr_date
    вернуть сумму всех этих значений, округлив до целых чисел
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'157790000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> count_act_for_one({'EUR': 56,'RUB': 330, 'TIEN': 150}, 157790000.0)
    12078
    '''
    list_of_values = [amount_of_actives[x] * price_for_date(str(curr_date),x) for x in amount_of_actives]
    return int(sum(list_of_values))



def diff_in_perc(fist_date: float, sec_date: float,amount_of_actives: Dict[str,int])-> float:
    '''
    Посчитать % разница между указанными датами

    на вход приходят fist_date: float, sec_date: float вида 1483826400.0,1593826400.0
    1) Необходимо посчитать стоимость портфеля с помощью функции count_act_for_one(amount_of_actives,curr_date),
    где amount_of_actives - входной параметр, а curr_date -fist_date
    2) Необходимо посчитать стоимость портфеля с помощью функции count_act_for_one(amount_of_actives,curr_date),
    где amount_of_actives - входной параметр, а curr_date - sec_date
    3) необходимо посчитать разницу в процентах между первым и вторым пунктом в десятичном виде , вернуть ее
    Например,
    результат по первому пункту - 10000 - 100%
    результат по второму пункут -8000 - x
    разница: -0.2

    результат по первому пункту - 9000 - 100%
    результат по второму пункут -10000 - x
    разница: 0.11
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'157790000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> diff_in_perc(1577892600.0, 157790000.0, {'EUR': 56,'RUB': 330, 'TIEN': 150})
    0.21
    >>> diff_in_perc(157790000.0, 1577892600.0, {'EUR': 56,'RUB': 330, 'TIEN': 150})
    -0.18
    '''
    first_date = count_act_for_one(amount_of_actives, fist_date)
    second_date = count_act_for_one(amount_of_actives, sec_date)
    return round((second_date/first_date)-1, 2)





def data_for_one_day(fist_date: float, sec_date: float, amount_of_actives: Dict[str,int]) -> Tuple[float, float, int]:
    '''
    Сбор всей информации для одной даты (дата, %, стоимость)

    Необходимо вернуть кортеж вида (sec_date, %, sum)
    1)sec_date - равен входному значению sec_date
    2)% равен результату функции diff_in_perc( fist_date, sec_date ,amount_of_actives), где все значения - входные значения
    3)sum равен результату функции count_act_for_one(amount_of_actives,sec_date ), где все значения - входные значения
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'157790000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> data_for_one_day(1577892600.0,157790000.0, {'EUR': 56,'RUB': 330, 'TIEN': 150})
    (157790000.0, 0.21, 12078)
    '''
    return tuple((sec_date, diff_in_perc(fist_date,sec_date,amount_of_actives), count_act_for_one(amount_of_actives,sec_date)))



def time_exists(date: float)-> bool:
    '''
    Функция проверяет на наличие даты в графе

    Необходимо проверить на пустоту узел с 'date': str(date),
    если пусто, то возвращается False, иначе True
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'1577979000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> time_exists(1577892600.0)
    True
    >>> time_exists(1677892600.0)
    False
    '''
    return gr.MatchOne({'date':str(date)}).get('type') != 'empty'





def data_for_all_dates(period_for_back:Tuple[float,float], amount_of_actives: Dict[str,int])-> List[Tuple[float, float, int]]:
    '''
    Формирует список всей информации на каждый день [(дата, %, стоимость),(дата, %, стоимость),...]

    На вход поступает period_for_back вида ('1234567890.0','1234567899.0')
    и amount_of_actives вида {'EUR':52,'RUB':30,....}
    Необходимо сформировать список кортежей, каждый кортеж представляет собой
    результат функции  data_for_one_day(fist_date, sec_date, amount_of_actives),
    где fist_date - это всегда первый элемент полученного резульатат функции period_for_back

    sec_date - это fist_date в первом кортеже, first_date + 86400 во втором кортеже, и sec_date+ 86400 в последующие разы
    amount_of_actives - входные данные функции
    !!!Каждый раз необходимо проверять sec_date с помощью time_exists(sec_date), если вовращает True, то элемент годится,
    иначе пропустить элемент и перейти к следующему(+86400)!!!

    Необходимо формировать кортежи до тех пор пока sec_date не станет
    больше или равен второго элемента полученного резульатат функции datestamp(period_for_back)
    Должен получиться список вида:
    [('1234567890.0', 0.0 , 100000 ),(),()]
    >>> gr = bs.Graph()
    >>> Node(gr, {'date':'1577892600.0', 'EUR':'60', 'RUB':'10', 'TIEN':'22'})
    >>> Node(gr, {'date':'1577979000.0', 'EUR':'63', 'RUB':'15', 'TIEN':'24'})
    >>> data_for_all_dates((1577892600.0,1577979000.0),{'EUR': 56,'RUB': 330, 'TIEN': 150})
    '''
    fist_date = period_for_back[0]
    result = []
    sec_date = fist_date
    while sec_date <= period_for_back[1]:
        print('in')
        print(sec_date)
        print(time_exists(sec_date))
        if time_exists(sec_date):
            print(sec_date)
            result += [data_for_one_day(fist_date, sec_date, amount_of_actives)]
        sec_date +=86400
    return result




def timestamp_to_date(timestamp: float) -> str:
    '''
    Преобразовывают время в формате timestamp в формат dd/mm/yyyy

    >>> timestamp_to_date(1483221600.0)
    '01/01/2017'
    '''
    return time.strftime("%d/%m/%Y %H/%M", time.gmtime(1483221600.0))









def make_graph(tup_for_all_dates:List[Tuple[float, float, int]])-> str:
    '''
    Функция строит график и сохраняет его, создает ссылку на нее

    На вход поступает tup_for_all_dates вида [('1234567890.0', 0.0 , 100000 ),('1234567899.0', 0.11 , 122000 ),(),...]
    Пл вертикальной оси откладывается третий элемент каждого кортежа, ось должна начинаться с отметки 0
    По горизонтальной оси откладываются первые элементы каждого кортежа, ось должна начинаться с первого элемента перовго кортежа
    По горизонтальной подписывать координату в формате  timestamp_to_date(timestamp),начиная с первого кортежа
    где timestamp равен первому элементу котежа
    Затем необходимо сохранить график и создать URL ссылку на него
    Вернуть ссылку
    '''
    xdata = list(map(lambda x: timestamp_to_date(x[0]) ,tup_for_all_dates))
    ydata = list(map(lambda x: x[2] ,tup_for_all_dates))
    #ydata = [x*2 for x in range(100)]

    fig = plot.Figure(data=plot.Scatter(x=xdata, y=ydata))

    image_name = str(uuid.uuid4()) + '.png'
    fig.update_layout(legend_orientation="h",
                  legend=dict(x=.5, xanchor="center"),
                  title="Change of price",
                  xaxis_title="Date",
                  yaxis_title="Price",
                  margin=dict(l=0, r=0, t=30, b=0))
    url = '/home/arcsin/stag/ivar2/static/'+ str(uuid.uuid4()) + '.html'
    fig.write_html(url)
    fig.write_image(image_name)
    #fig.show()
    return 'file://' + url






def return_backtest(period_for_back:Tuple[str,str], invested_sum: float,result: Dict[str,List[Dict[str,str]]]) -> Dict[str, Union[str,float, List[Tuple[float, float, int]]]]:
    """Возвращает backtest для определенного портфеля
        На вход поступает  period_for_back вида ('01/01/2017', '01/01/2018')
        invested sum вида 10000.0
        result вида {'forex':[{'name': 'EUR', 'type':'','country':''},{'name': 'RUB', 'type':'','country':''}],'crypto': [{'name': 'buxcoin', 'type':'','country':''}], 'company':[]}
        1)Необходимо вызвать функцию csv_into_nodelist(csvfilename),где  csvfilename - входной параметр
        2)Вызвать функцию get_asset(result), результатом является asset
        3)Вызвать функцию datestamp(period_for_back), результатом является кортеж из двух чисел float (float1, float2)
        4)Вызвать функцию amount_of_actives(date, invested_sum, asset),где date - первое значение кортежа из п. 3) -т,е, float1, invested_sum - входной параметр, asset - результат п.2),
        результатом будет являться amount_of_actives
        5)Вызвать функцию data_for_all_dates(period_for_back, amount_of_actives), где period_for_back - рез-т п.3) , amount_of_actives -рез-т п.4)
        результатом будет являться tup_for_all_dates
        6) Вызвать функцию make_graph(tup_for_all_dates), tup_for_all_dates - рез п.5)результатом будет являться url
        7) Вызвать функцию diff_in_perc(fist_date, sec_date,amount_of_actives),
        где fist_date,sec_date - первый и второй элемент результата- кортежа из п.2) соотвественно,amount_of_actives результат из п.4)резульатом будетявляться comm_perc
        Функция возвращает словарь вида {'perc_common' : comm_perc, "perc_daily": tup_for_all_dates ,'URL': url}, где comm_perc - резульатат п.7) tup_for_all_dates - результат п.5) url - результат п.6)
    """

    #1
    
    #2
    asset = get_asset(result)
    #3
    tuple_float = datestamp(period_for_back)
    #4
    amount = amount_of_actives(tuple_float[0], invested_sum, asset)
    #5
    tup_for_all_dates = data_for_all_dates(tuple_float, amount)
    #6
    url = make_graph(tup_for_all_dates)
    #7
    comm_perc = diff_in_perc(tuple_float[0], tuple_float[1], amount)
    print({"perc_common": comm_perc, "perc_daily": tup_for_all_dates, 'URL': url})
    return {"perc_common": comm_perc, "perc_daily": tup_for_all_dates, 'URL': url}




if __name__ == "__main__":
    gr = bs.Graph()
    Node(gr, {'date': '1577826000.0', 'EUR': '60', 'RUB': '10', 'TIEN': '22'})
    Node(gr, {'date': '1578171600.0', 'EUR': '63', 'RUB': '15', 'TIEN': '24'})
    Node(gr, {'date': '1578258000.0', 'EUR': '75', 'RUB': '52', 'TIEN': '100'})
    Node(gr, {'date': '1578344400.0', 'EUR': '100', 'RUB': '25', 'TIEN': '77'})
    return_backtest(('01/12/2019', '01/02/2020'),10000.0,{'forex':[{'name': 'EUR', 'type':'','country':''},{'name': 'RUB', 'type':'','country':''}],'crypto': [{'name': 'TIEN', 'type':'','country':''}], 'company':[]} )






